//TEMPLATE LITERALS
let getCube = Math.pow(2,3)
console.log(`The cube of 2 is ${getCube}`)


//DESTRUCTURING ARRAY

const address = ["258", "Washington Ave NW", "California", "90011"]

let [street, avenue, state, zip] = address

console.log(`I live at ${street} ${avenue}, ${state}, ${zip}`)


//DESTRUCTURING OBJECT

const animal = {
    Name: "Lolong",
    type: "saltwater crocodile",
    weight: "1075 kgs",
    measurement: "20 ft 3 in"
}

let = {Name, type, weight, measurement}= animal

console.log(`${Name} was a ${type}. He weighed at ${weight} with a measurement of ${measurement}.`)

//IMPLICIT RETURN STATEMENT

const num = [1,2,3,4,5]

const numloop = Object.values(num)

numloop.forEach(function(nums){
    console.log(`${nums}`)
})

let reduceNumber = (a,b) => parseInt(a) + parseInt(b);
console.log(num.reduce(reduceNumber))
